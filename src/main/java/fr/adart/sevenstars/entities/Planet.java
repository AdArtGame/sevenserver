package fr.adart.sevenstars.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "planets")
public class Planet implements Serializable, IEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "slot")
    private int slot;

    @Column(name = "percent_metal")
    private int percentMetal;

    @Column(name = "percent_chemical")
    private int percentChemical;

    @Column(name = "percent_radio")
    private int percentRadio;

    @Column(name = "is_alive")
    private boolean isAlive;

    @Column(name = "png")
    private String png;

    @ManyToOne
    @JoinColumn(name = "star_id", nullable = false)
    private Star star;

    public Planet() {
    }

    public Planet(int slot, int percentMetal, int percentChemical, int percentRadio, boolean isAlive, String png, Star star) {
        this.slot = slot;
        this.percentMetal = percentMetal;
        this.percentChemical = percentChemical;
        this.percentRadio = percentRadio;
        this.isAlive = isAlive;
        this.png = png;
        this.star = star;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getSlot() {
        return slot;
    }

    public void setSlot(int slot) {
        this.slot = slot;
    }

    public int getPercentMetal() {
        return percentMetal;
    }

    public void setPercentMetal(int percentMetal) {
        this.percentMetal = percentMetal;
    }

    public int getPercentChemical() {
        return percentChemical;
    }

    public void setPercentChemical(int percentChemical) {
        this.percentChemical = percentChemical;
    }

    public int getPercentRadio() {
        return percentRadio;
    }

    public void setPercentRadio(int percentRadio) {
        this.percentRadio = percentRadio;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setIsAlive(boolean isAlive) {
        this.isAlive = isAlive;
    }

    public String getPng() {
        return png;
    }

    public void setPng(String png) {
        this.png = png;
    }

    public Star getStar() {
        return star;
    }

    public void setStar(Star star) {
        this.star = star;
    }
}
