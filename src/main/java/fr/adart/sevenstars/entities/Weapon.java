package fr.adart.sevenstars.entities;

import javax.persistence.Entity;

@Entity(name = "ships_weapons")
public class Weapon extends CoupleXY {

    public Weapon() {
    }

    public Weapon(int x, int y) {
        super(x, y);
    }
}
