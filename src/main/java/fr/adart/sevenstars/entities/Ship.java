package fr.adart.sevenstars.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "ships")
public class Ship implements Serializable, IEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "model", nullable = false)
    private String model;

    @Column(name = "hp", nullable = false)
    private String hp;

    @Column(name = "agility", nullable = false)
    private Integer agility;

    @ElementCollection
    @CollectionTable(name = "ships_shields", joinColumns = @JoinColumn(name = "ship_id"))
    @Column(name = "shield")
    private List<Integer> shield = new ArrayList<>(3);

    @ElementCollection
    @CollectionTable(name = "ships_coolings", joinColumns = @JoinColumn(name = "ship_id"))
    @Column(name = "cooling")
    private List<Integer> cooling = new ArrayList<>(3);

    @OneToMany(mappedBy = "ship", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Weapon> weapons = new ArrayList<>();

    @OneToMany(mappedBy = "ship", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Util> utils = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getHp() {
        return hp;
    }

    public void setHp(String hp) {
        this.hp = hp;
    }

    public Integer getAgility() {
        return agility;
    }

    public void setAgility(Integer agility) {
        this.agility = agility;
    }

    public List<Integer> getShield() {
        return shield;
    }

    public void setShield(List<Integer> shield) {
        this.shield = shield;
    }

    public List<Integer> getCooling() {
        return cooling;
    }

    public void setCooling(List<Integer> cooling) {
        this.cooling = cooling;
    }

    public List<Weapon> getWeapons() {
        return weapons;
    }

    public void setWeapons(List<Weapon> weapons) {
        this.weapons = weapons;
    }

    public List<Util> getUtils() {
        return utils;
    }

    public void setUtils(List<Util> utils) {
        this.utils = utils;
    }
}
