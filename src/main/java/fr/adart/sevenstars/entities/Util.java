package fr.adart.sevenstars.entities;

import javax.persistence.Entity;

@Entity(name = "ships_utils")
public class Util extends CoupleXY {

    public Util() {
    }

    public Util(int x, int y) {
        super(x, y);
    }
}
