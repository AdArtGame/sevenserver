package fr.adart.sevenstars.controllers;

import fr.adart.sevenstars.services.InitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InitController {

    @Autowired
    InitService initService;

    @RequestMapping("/init")
    public void init() {
        initService.init();
    }

}
