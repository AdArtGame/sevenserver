package fr.adart.sevenstars.controllers;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class WebSocketController {

    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public String test(String hello) throws InterruptedException {
        Thread.sleep(1000); // simulated delay
        return "Hello, " + hello + "!";
    }
}
