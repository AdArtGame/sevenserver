package fr.adart.sevenstars.repositories;

import fr.adart.sevenstars.entities.Ship;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShipRepository extends JpaRepository<Ship, Long> {

}
