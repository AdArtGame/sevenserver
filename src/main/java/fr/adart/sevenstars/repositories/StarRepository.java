package fr.adart.sevenstars.repositories;

import fr.adart.sevenstars.entities.Star;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StarRepository extends JpaRepository<Star, Long> {

}
