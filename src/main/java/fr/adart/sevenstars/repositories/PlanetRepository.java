package fr.adart.sevenstars.repositories;

import fr.adart.sevenstars.entities.Planet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlanetRepository extends JpaRepository<Planet, Long> {

}
