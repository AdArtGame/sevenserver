package fr.adart.sevenstars.repositories;

import fr.adart.sevenstars.entities.Player;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlayerRepository extends JpaRepository<Player, Long> {

}
