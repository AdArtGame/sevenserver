package fr.adart.sevenstars.repositories;

import fr.adart.sevenstars.entities.Galaxy;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GalaxyRepository extends JpaRepository<Galaxy, Long> {

}
