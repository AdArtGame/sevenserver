package fr.adart.sevenstars.services;

import fr.adart.sevenstars.entities.IEntity;

import java.util.List;

public interface AbtractCrudService<T extends IEntity> {

    T get(Long id);

    List<T> getAll();

    T create(T entity);

    T update(T entity);

    void delete(Long id);
}
