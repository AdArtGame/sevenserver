package fr.adart.sevenstars.services.impls;

import fr.adart.sevenstars.entities.Star;
import fr.adart.sevenstars.services.ShipService;
import org.springframework.stereotype.Component;

@Component
public class StarServiceImpl extends AbtractCrudServiceImpl<Star> implements ShipService {

}
