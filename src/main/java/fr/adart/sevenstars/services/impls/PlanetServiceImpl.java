package fr.adart.sevenstars.services.impls;

import fr.adart.sevenstars.entities.Planet;
import fr.adart.sevenstars.services.PlanetService;
import org.springframework.stereotype.Component;

@Component
public class PlanetServiceImpl extends AbtractCrudServiceImpl<Planet> implements PlanetService {

}