package fr.adart.sevenstars.services.impls;

import fr.adart.sevenstars.entities.Ship;
import fr.adart.sevenstars.services.ShipService;
import org.springframework.stereotype.Component;

@Component
public class ShipServiceImpl extends AbtractCrudServiceImpl<Ship> implements ShipService {

}
