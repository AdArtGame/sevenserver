package fr.adart.sevenstars.services.impls;

import fr.adart.sevenstars.entities.Galaxy;
import fr.adart.sevenstars.entities.Star;
import fr.adart.sevenstars.repositories.GalaxyRepository;
import fr.adart.sevenstars.repositories.PlanetRepository;
import fr.adart.sevenstars.repositories.PlayerRepository;
import fr.adart.sevenstars.repositories.StarRepository;
import fr.adart.sevenstars.services.InitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class InitServiceImpl implements InitService {

    @Autowired
    private GalaxyRepository galaxyRepository;

    @Autowired
    private StarRepository starRepository;

    @Autowired
    private PlanetRepository planetRepository;

    @Autowired
    private PlayerRepository playerRepository;

    @Override
    public void init() {
        System.out.println("---------- START INIT ----------");

        Galaxy g0 = new Galaxy();
        Galaxy g1 = new Galaxy();
        galaxyRepository.save(g0);
        galaxyRepository.save(g1);

        Star star0 = new Star(-350f, -310f, "Haru", g0);
        Star star1 = new Star(25f, -220f, "Taurus-12", g0);
        Star star2 = new Star(-200f, -40f, "Utu", g0);
        Star star3 = new Star(200f, 20f, "Surya-00", g0);
        Star star4 = new Star(20f, 225f, "Youta", g0);
        Star star5 = new Star(400f, -100f, "Arth-01", g0);
        Star star6 = new Star(-250f, 350f, "Eren", g0);
        Star star7 = new Star(650f, -340f, "Sorin", g0);
        Star star8 = new Star(830f, -10f, "Veg-99", g0);
        Star star9 = new Star(1000f, -180f, "Oota", g0);
        Star star10 = new Star(350f, 200f, "Natj-52", g0);
        Star star11 = new Star(-100f, 600f, "Mah", g0);
        Star star12 = new Star(250f, 430f, "Senay", g0);
        Star star13 = new Star(400f, 780f, "Gim-04", g0);
        Star star14 = new Star(500f, 630f, "Kihn", g0);
        Star star15 = new Star(700f, 350f, "Almika", g0);
        Star star16 = new Star(900f, 300f, "Lani", g0);
        Star star17 = new Star(1000f, 540f, "Hilda-00", g0);
        Star star18 = new Star(800f, 780f, "Khaa", g0);
        Star star19 = new Star(150f, 700f, "Budur", g0);
        Star star20 = new Star(625f, 580f, "Pix-42", g0);

        Star star21 = new Star(-350f, -310f, "Nivh", g1);
        Star star22 = new Star(25f, -220f, "Taurus-14", g1);
        Star star23 = new Star(-200f, -40f, "Carnia", g1);
        Star star24 = new Star(200f, 20f, "Lian-14", g1);
        Star star25 = new Star(20f, 225f, "Youto", g1);
        Star star26 = new Star(400f, -100f, "Arth-02", g1);
        Star star27 = new Star(-250f, 350f, "Arka-1", g1);
        Star star28 = new Star(650f, -340f, "Cyrus", g1);
        Star star29 = new Star(830f, -10f, "Punk-00", g1);
        Star star30 = new Star(1000f, -180f, "Etu", g1);
        Star star31 = new Star(350f, 200f, "Jerioth", g1);
        Star star32 = new Star(-100f, 600f, "Muta-66", g1);
        Star star33 = new Star(250f, 430f, "Leera", g1);
        Star star34 = new Star(400f, 780f, "Lene", g1);
        Star star35 = new Star(500f, 630f, "Xia-lh", g1);
        Star star36 = new Star(700f, 350f, "Nes-2", g1);
        Star star37 = new Star(900f, 300f, "Sondo", g1);
        Star star38 = new Star(1000f, 540f, "Hilda-01", g1);
        Star star39 = new Star(800f, 780f, "Suke", g1);
        Star star40 = new Star(150f, 700f, "Yoma", g1);
        Star star41 = new Star(625f, 580f, "Pax-42", g1);

        starRepository.save(Arrays.<Star>asList(
            star0, star1, star2, star3, star4, star5, star5, star6, star7, star8, star9,
            star10, star11, star12, star12, star13, star14, star15, star16, star17, star18, star19,
            star20, star21, star22, star23, star24, star25, star25, star26, star27, star28, star29,
            star30, star31, star32, star33, star34, star35, star36, star37, star38, star39,
            star40, star41));

        System.out.println("----------  END INIT  ----------");
    }
}
