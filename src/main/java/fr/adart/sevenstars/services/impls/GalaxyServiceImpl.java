package fr.adart.sevenstars.services.impls;

import fr.adart.sevenstars.entities.Galaxy;
import fr.adart.sevenstars.services.GalaxieService;
import org.springframework.stereotype.Component;

@Component
public class GalaxyServiceImpl extends AbtractCrudServiceImpl<Galaxy> implements GalaxieService {

}
