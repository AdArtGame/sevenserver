package fr.adart.sevenstars.services.impls;

import fr.adart.sevenstars.entities.Player;
import fr.adart.sevenstars.services.PlanetService;
import org.springframework.stereotype.Component;

@Component
public class PlayerServiceImpl extends AbtractCrudServiceImpl<Player> implements PlanetService {

}
