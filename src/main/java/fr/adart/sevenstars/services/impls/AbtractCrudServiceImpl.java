package fr.adart.sevenstars.services.impls;

import fr.adart.sevenstars.entities.IEntity;
import fr.adart.sevenstars.services.AbtractCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
public abstract class AbtractCrudServiceImpl<T extends IEntity> implements AbtractCrudService<T> {

    @Autowired
    protected JpaRepository<T, Long> repository;

    @Override
    @Transactional(readOnly = true)
    public T get(Long id) {
        return repository.findOne(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<T> getAll() {
        return repository.findAll();
    }

    @Override
    @Transactional
    public T create(T entity) {
        return repository.save(entity);
    }

    @Override
    @Transactional
    public T update(T entity) {
        return repository.save(entity);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        repository.delete(id);
    }
}
